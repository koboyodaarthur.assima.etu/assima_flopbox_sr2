# Projet 1 SR2

- Assima Arthur

## Description 

L'objectif du projet est de développer la plate-forme FlopBox en adoptant le style architectural REST pour nous permettre de centraliser la gestion de fichiers distants stockés dans des serveurs FTP tiers


# Utilisation 

```code
cd flopbox
```

```code
mvn clean compile
```

puis

```code
mvn exec:java
```

- géneration de la javadoc :

```code
mvn javadoc:javadoc
```

## Commandes

Affichage des serveurs disponibles :

```code
curl -X GET http://localhost:8080/flopbox/servers
```

Ajout d'un serveur :

```code
curl -X POST http://localhost:8080/flopbox/servers/nom?adrs=addresse
```

Modification d'un serveur :

```code
curl -X PUT http://localhost:8080/flopbox/servers/nom?adrs=nouvelle_addresse
```

Suppression d'un serveur :

```code
curl -X DELETE http://localhost:8080/flopbox/server/nom
```

Affichage d'un répertoire :

```code
curl -X GET http://localhost:8080/flopbox/server/nom[?path=&port=&user=&pswd=]
```



## Architecture


On dispose d'une classe main et d'un package flopbox.resources avec deux resources
- Servers :
Permet d'agir sur les différents serveurs(nom + addresse) de la plateforme (ajout,modification,suppression)

- Server :
Permet d'agir sur un serveur en particulier (listage,récupération de fichier, etc.)


## Code Samples

- Exécution du listage de fichier d'un répertoire avec détection d'erreurs

```code
try {
			if (path != null)
				files = ftp.listNames(path);
			else
				files = ftp.listNames();
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			return "400 Error, dir not found, try again \n";
		}

		for (String file : files) {
			res += file + "\n";
		}
```

- Initialisation d'une conection selon les paramètres de la requête

```code
if (port == null) {
			try {
				ftp.connect(Servers.getAddress(name));
				boolean b = ftp.login(user, password);
				if (!b)
					return "400 Invalid user/password.";

			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		} else {
			int p = Integer.parseInt(port);
			try {
				ftp.connect(Servers.getAddress(name), p);
				boolean b = ftp.login(user, password);
				if (!b)
					return "400 Invalid user/password.";
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
```
