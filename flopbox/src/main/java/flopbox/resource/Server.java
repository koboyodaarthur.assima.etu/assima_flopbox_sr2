package flopbox.resource;

import java.io.*;
import java.net.SocketException;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.net.ftp.*;

// TODO: Auto-generated Javadoc
/**
 * Resource dealing with a specific Server.
 * @author Arthur Assima
 */
@Path("server")
public class Server {
	
	/**
	 * Lists the files in a directory.
	 *
	 * @param name the name
	 * @param user the user
	 * @param password the password
	 * @param port the port
	 * @param path the path
	 * @return the string
	 */
	@GET
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String list(@PathParam("name") String name, @QueryParam("user") String user,
			@QueryParam("pswd") String password, @QueryParam("port") String port, @QueryParam("path") String path) {
		String[] files;
		String res = "";
		FTPClient ftp = new FTPClient();
		String c = connect(ftp, name, user, password, port);
		if (c != "") {
			return c;
		}
		try {
			if (path != null)
				files = ftp.listNames(path);
			else
				files = ftp.listNames();
			ftp.logout();
			ftp.disconnect();
		} catch (IOException e) {
			return "400 Error, dir not found, try again \n";
		}

		for (String file : files) {
			res += file + "\n";
		}
		if (res == "") {
			return "400 Error while listing, try again. \n";
		}
		return "200 Directory successfully listed.\nFiles : \n" + res;
	}

	/**
	 * Gets the file from the FTP server. (Incomplete)
	 *
	 * @param name the name of the server
	 * @param path the path to the file
	 * @param user the user
	 * @param password the password
	 * @param port the port
	 * @return the response (done or error) 
	 */
	@GET
	@Path("/{name}/file/{path: .*}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public String getFileFTP(@PathParam("name") String name, @PathParam("path") String path,
			@QueryParam("user") String user, @QueryParam("pswd") String password, @QueryParam("port") String port) {
		String res;
		FTPClient ftp = new FTPClient();
		String c = connect(ftp, name, user, password, port);
		File file = new File("/ftp/" + path);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			try {
				file.createNewFile();
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
		ftp.enterLocalPassiveMode();
		try {
			OutputStream os = new FileOutputStream(file);
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			ftp.retrieveFile(path, os);
			res = "200 The file has been succesfully downloaded.";
			ftp.logout();
			ftp.disconnect();
		} catch (Exception e) {
			res = "400 Error while downloading the file.\n";
		}
		return res;

	}

	/**
	 * Connects with the FTP server.
	 *
	 * @param ftp the ftp server
	 * @param name the name of the flopbox's server
	 * @param user the user
	 * @param password the password
	 * @param port the port
	 * @return the response (done or error)
	 */
	private String connect(FTPClient ftp, String name, String user, String password, String port) {
		if (user == null) {
			user = "anonymous";
		}
		if (password == null) {
			password = "anonymous";
		}
		if (port == null) {
			try {
				ftp.connect(Servers.getAddress(name));
				boolean b = ftp.login(user, password);
				if (!b)
					return "400 Invalid user/password.";

			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		} else {
			int p = Integer.parseInt(port);
			try {
				ftp.connect(Servers.getAddress(name), p);
				boolean b = ftp.login(user, password);
				if (!b)
					return "400 Invalid user/password.";
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
		return "";
	}

}
