package flopbox.resource;

import java.util.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;


/**
 * Resource dealing with servers
 * @author Arthur Assima
 */
@Path("/servers")
public class Servers {
	private static Map<String, String> servers = new HashMap<>();


	/**
	 * Adds the server to the platform.
	 *
	 * @param name the name
	 * @param address the address
	 * @return the string
	 */
	@POST
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String addServer(@PathParam("name") String name, @QueryParam("adrs") String address) {
		if (servers.containsKey(name) || name == null || address == null) {
			return "400 Error while adding, try again.";
		}
		servers.put(name, address);
		return "200 Server " + name + " added.";
	}

	/**
	 * Modify a server.
	 *
	 * @param name the name
	 * @param address the new address
	 * @return the string
	 */
	@PUT
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String modifyServer(@PathParam("name") String name, @QueryParam("adrs") String address) {
		if (!servers.containsKey(name) || name == null || address == null) {
			return "400 Error while updating, try again.";
		}
		servers.remove(name);
		servers.put(name, address);
		return "200 Server " + name + " updated.";
	}

	/**
	 * Delete a server.
	 *
	 * @param name the name of the server
	 * @return the response (done or error)
	 */
	@DELETE
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteServer(@PathParam("name") String name) {
		if (!servers.containsKey(name) || name == null) {
			return "400 Error while deleting, try again.";
		}
		servers.remove(name);
		return "200 Server " + name + " deleted.";
	}

	/**
	 * Gets all the servers.
	 *
	 * @return all the servers on the platform
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getServers() {
		if (servers.isEmpty()) {
			return "No servers yet.";
		}
		String res = "";
		for (Map.Entry<String, String> server : servers.entrySet()) {
			res += "Name : " + server.getKey() + " Address : " + server.getValue() + "\n";
		}
		return res;
	}
	
	/**
	 * Gets the address of a server.
	 *
	 * @param name the name
	 * @return the address
	 */
	public static String getAddress (String name) {
		return servers.get(name);
	}

}
