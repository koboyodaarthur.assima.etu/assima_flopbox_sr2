echo Liste serveurs --------------------------
curl -X GET http://localhost:8080/flopbox/servers

echo 
echo Ajout free ------------------------------
curl -X POST http://localhost:8080/flopbox/servers/free?adrs=ftp.free.fr

echo 
echo Ajout python ----------------------------
curl -X POST http://localhost:8080/flopbox/servers/python?adrs=0.0.0.0&port=2121


echo 
echo Ajout ubuntu ----------------------------
curl -X POST http://localhost:8080/flopbox/servers/ubuntu?adrs=ftp

echo 
echo Liste serveurs --------------------------
curl -X GET http://localhost:8080/flopbox/servers

echo 
echo Modification ubuntu ---------------------
curl -X PUT http://localhost:8080/flopbox/servers/ubuntu?adrs=ftp.ubuntu.com

echo
echo Listing ubuntu/indices ------------------
curl -X GET http://localhost:8080/flopbox/server/ubuntu?path=./ubuntu/indices

echo 
echo Suppression serveur python --------------
curl -X DELETE http://localhost:8080/flopbox/server/python

echo 
echo Liste serveurs --------------------------
curl -X GET http://localhost:8080/flopbox/servers